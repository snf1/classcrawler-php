<?php

namespace Tools\CodeBase;

use PhpParser\Lexer\Emulative;
use PhpParser\ParserFactory;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node;
use PhpParser\ NodeFinder;
use PhpParser\NodeVisitorAbstract;
use PhpParser\BuilderFactory;
use PhpParser\PrettyPrinter;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\Yield_;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ArrayItem;
use PhpParser\Node\Stmt\Return_;

class TestGenerator {
	public function generateTest( $file ) {
		$phpParser = ( new ParserFactory )->create(
			ParserFactory::PREFER_PHP7,
			new Emulative( [ 'usedAttributes' => [
				'comments', 'startLine', 'endLine',
				'startFilePos',
				'endFilePos'
			] ] )
		);
		$code = file_get_contents( $file );

		try {
			$nodeFinder = new NodeFinder;
			$nodes = $phpParser->parse( $code );
			$classToTests = $nodeFinder->findFirst( $nodes, function ( Node $node )  {
				if ( $node instanceof Class_ ) {
					return $node->name->name === "ContentHandler";
				}
				return false;
			} );

			$functionToTest = $nodeFinder->findFirst( $classToTests->stmts, function ( Node $node )  {
				if ( $node instanceof ClassMethod ) {
					return $node->name->name === "getPageViewLanguage";
				}
				return false;
			} );

		return $this->generateTestMeta( new FunctionToTest( $functionToTest ) );

		} catch ( Exception $ex ) {
			print( 'Error on file: ' . $file );
		}

		return "";
	}

	private function combineTestData(): array {
		$namespaces = [ 'NS_FILE', 'NS_MEDIAWIKI' ];
		$languages = [ 'sr', 'sr-ec' ];
		$variants = [ 'sr', 'sr-ec' ];

		$factory = new BuilderFactory;

		$res = [];
		foreach ( $namespaces as $namespace ) {
			foreach ( $languages as $lang ) {
				foreach ( $variants as $variant ) {
					$res[] = [
						new ArrayItem( $factory->constFetch( $namespace ) ),
						new ArrayItem( $factory->val( $lang ) ),
						new ArrayItem( $factory->val( $variant ) ),
						new ArrayItem( $factory->val( '' ) )
					];
				}
			}
		}
		return $res;
	}

	private function generateTestProviderMeta( FunctionToTest $functionToTest ) {
		$providerSS = 'provider' . ucfirst( $functionToTest->name() );
		$factory = new BuilderFactory;
		$provider = $factory->method( $providerSS )
			->makePublic();
		foreach ( $this->combineTestData() as $data ) {
			$provider->addStmt( new Yield_( new Array_( $data ) ) );
		}

		return $provider;
	}

	private function generateMockMeta( FunctionToTest $functionToTest ) {
		$getMockMethod = 'getMock' . ucfirst( $functionToTest->className() );
		$factory = new BuilderFactory;

		$mockCall = $factory->methodCall(
			$factory->methodCall(
				$factory->methodCall(
					$factory->var( 'this' ),
					'getMockBuilder',
					[ $factory->classConstFetch( 'ContentHandler', 'class' ) ]
				),
				'disableOriginalConstructor'
			),
			'getMockForAbstractClass'
		);

		$provider = $factory->method( $getMockMethod )
			->makePrivate()
			->addStmt( new Assign( $factory->var( 'handler' ), $mockCall ) )
			->addStmt( new Return_( $factory->var( 'handler' ) ) );

		return $provider;
	}

	private function generateTestMeta( FunctionToTest $functionToTest ) {
		$factory = new BuilderFactory;

		$args = $functionToTest->arguments();

		$testClass = $factory->class( $functionToTest->className() . 'GeneratedTest' )
			->extend( 'MediaWikiTestCase' )
			->setDocComment( '/**
												 * @coversDefaultClass ' . $functionToTest->className() . '
												 */' )
			->addStmt( $this->generateTestProviderMeta( $functionToTest ) )
			->addStmt( $this->generateMockMeta( $functionToTest ) )
			->addStmt( $factory->method( 'test' . ucfirst( $functionToTest->name() ) )
				->makePublic()
				->addParam( $factory->param( 'namespace' )->setType( 'int' ) )
				->addParam( $factory->param( 'lang' )->setType( 'string' ) )
				->addParam( $factory->param( 'variant' )->setType( 'string' ) )
				->addParam( $factory->param( 'expected' )->setType( 'string' ) )
				->setDocComment( '/**
													* @dataProvider provider' . ucfirst( $functionToTest->name() ) . '
													* @cover ::' . $functionToTest->name() . '
													*/' )
				->addStmt( new Assign(
					$factory->var( lcfirst( $functionToTest->className() ) ),
					$factory->methodCall( $factory->var( 'this' ), 'getMock' . $functionToTest->className() ) )
				)
				->addStmt( $factory->methodCall(
					$factory->var( 'this' ),
					'setMwGlobals',
					[
						[ 'wgDefaultLanguageVariant' => $factory->var( 'variant' )]
					]
				) )
				->addStmt( $factory->methodCall(
					$factory->var( 'this' ),
					'setUserLang',
					[
						$factory->var( 'lang' )
					]
				) )
				->addStmt( $factory->methodCall(
					$factory->var( 'this' ),
					'setContentLang',
					[
						$factory->var( 'lang' )
					]
				) )
				->addStmt( new Assign(
					$factory->var( 'title' ),
					$factory->staticCall( 'Title', 'newFromText', [ 'TitleForTests', 	$factory->var( 'namespace' ) ] )
				) )
				->addStmt( new Assign(
					$factory->var( 'result' ),
					$factory->methodCall(
						$factory->var( lcfirst( $functionToTest->className() ) ),
						$functionToTest->name(),
						[
							$factory->var( 'title' )
						]
					) ) )
				->addStmt( $factory->methodCall(
					$factory->var( 'this' ),
					'assertEquals',
					[
						$factory->var( 'expected' ),
						$factory->methodCall( $factory->var( 'result' ), 'getCode' )
					]
				) )
			);

		$node =	$testClass->getNode();

		$stmts = [ $node ];
		$prettyPrinter = new PrettyPrinter\Standard();
		return $prettyPrinter->prettyPrintFile( $stmts );
	}
}
