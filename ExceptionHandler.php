<?php

namespace Tools;

use ErrorException;

class ExceptionHandler {
    public static function handleError(
        $level,
        $message,
        $file = null,
        $line = null
    ) {
        switch ( $level ) {
            case E_WARNING:
            case E_CORE_WARNING:
            case E_COMPILE_WARNING:
                $prefix = 'PHP Warning: ';
                break;
            case E_NOTICE:
                $prefix = 'PHP Notice: ';
                break;
            case E_USER_NOTICE:
                $prefix = 'PHP Notice: ';
                break;
            case E_USER_WARNING:
                $prefix = 'PHP Warning: ';
                break;
            case E_STRICT:
                $prefix = 'PHP Strict Standards: ';
                break;
            case E_DEPRECATED:
                $prefix = 'PHP Deprecated: ';
                break;
            case E_USER_DEPRECATED:
                $prefix = 'PHP Deprecated: ';
                break;
            default:
                $prefix = 'PHP Unknown error: ';
                break;
        }

        throw new ErrorException( $prefix . $message, 0, $level, $file, $line );
    }

    protected static $fatalErrorTypes = [
		E_ERROR,
		E_PARSE,
		E_CORE_ERROR,
		E_COMPILE_ERROR,
		E_USER_ERROR,

		// E.g. "Catchable fatal error: Argument X must be Y, null given"
		E_RECOVERABLE_ERROR,
	];

    /** @var string */
	protected static $reservedMemory;

    public static function handleFatalError() {
		// Free reserved memory so that we have space to process OOM
		// errors
		self::$reservedMemory = null;

		$lastError = error_get_last();
		if ( $lastError === null ) {
			return false;
		}

		$level = $lastError['type'];
		$message = $lastError['message'];
		$file = $lastError['file'];
		$line = $lastError['line'];

		if ( !in_array( $level, self::$fatalErrorTypes ) ) {
			// Only interested in fatal errors, others should have been
			// handled by MWExceptionHandler::handleError
			return false;
		}

		$msgParts = [
			'[{reqId}] {exception_url}   PHP Fatal Error',
			( $line || $file ) ? ' from' : '',
			$line ? " line $line" : '',
			( $line && $file ) ? ' of' : '',
			$file ? " $file" : '',
			": $message",
		];
		$msg = implode( '', $msgParts );

		// Look at message to see if this is a class not found failure (Class 'foo' not found)
		if ( preg_match( "/Class '\w+' not found/", $message ) ) {
			// phpcs:disable Generic.Files.LineLength
			$msg = <<<TXT
{$msg}

MediaWiki or an installed extension requires this class but it is not embedded directly in MediaWiki's git repository and must be installed separately by the end user.

Please see <a href="https://www.mediawiki.org/wiki/Download_from_Git#Fetch_external_libraries">mediawiki.org</a> for help on installing the required components.
TXT;
			// phpcs:enable
		}

		throw new ErrorException( "PHP Fatal Error: {$message}", 0, $level, $file, $line );

	}


    public static function installHandler() {
        set_error_handler( 'Tools\ExceptionHandler::handleError' );
        //self::$reservedMemory = str_repeat( ' ', 16384 );
		//register_shutdown_function( 'Tools\ExceptionHandler::handleFatalError' );
    }
}
