<?php

namespace Tools\CodeBase;

use Tools\CodeBase\Project;


interface File {
	public function name(): string;

	public function path(): string;

	public function project(): Project;
}
