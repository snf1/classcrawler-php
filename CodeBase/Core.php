<?php

namespace Tools\CodeBase;

use Traversable;
use AppendIterator;
use Iterator;
use OuterIterator;
use RecursiveArrayIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Tools\CodeBase\Files\GenericFileIterator;

// interface PhpBlock {
// 	public function parent();

// 	public function blocks();

// 	public function doc();
// }

class PhpFileInfo {

	public function __construct( string $basePath ) {
		$this->basePath = $basePath;
	}

	public function getDoc( $doc ) {
		$this->doc = $doc;
	}

	public function classes(): array {
		return [];
	}

}

// class PhpMethodInfo {
// 	private $parent;
// 	private $doc = '';
// 	private $blocks = [];
// }

class PhpClassInfo {

	private $node;

	public function __construct(
		object $node
	) {
		$this->node = $node;
	}

	function getParent() : PhpClassInfo {
		return $node->get_parent_class();
	}

	function getName() {

	}
	function getNamespaceName() {

	}

	function getMethods() {

	}
	function getProperies() {
		
	}

}

// class SSSVisitor extends \PhpParser\NodeVisitorAbstract {

// 	private $current;

// 	public function enterNode( \PhpParser\Node $node ) {
// 		if ( $node instanceof \PhpParser\Node\Stmt\ClassLike ) {
// 			$this->current = new PhpClassInfo( $this->current );
// 		}

// 		if ( $node instanceof \PhpParser\Node\Stmt\Namespace_ ) {
// 			$this->setNamespace( $node->name->getName() );
// 		}

// 		return null;
// 	}

// 	public function leaveNode( \PhpParser\Node $node ) {
// 		if ( $node instanceof \PhpParser\Node\Stmt\ClassLike ) {
// 			$this->current = $this->current->parent();
// 		}
// 	}
// }

// class PhpFileParser {
// 	private $parser;

// 	public function __construct() {
// 		$this->parser = ( new \PhpParser\ParserFactory )->create(
// 			\PhpParser\ParserFactory::PREFER_PHP7,
// 			new \PhpParser\Lexer\Emulative( [ 'usedAttributes' => [
// 				'comments', 'startLine', 'endLine',
// 				'startFilePos',
// 				'endFilePos'
// 			] ] )
// 		);
// 	}
// }

class Core implements Project {

	private $basePath;

	public function __construct( string $basePath ) {
		$this->basePath = $basePath;
	}

	public function files(): Iterator {
	
		$files = new AppendIterator();
		
		$includes = new RecursiveDirectoryIterator( $this->basePath . '/includes' );
		$files->append( new RecursiveIteratorIterator( $includes ) );
	
		$language = new RecursiveDirectoryIterator( $this->basePath . '/languages' );
		$files->append( new RecursiveIteratorIterator( $language ) );
		
		return new GenericFileIterator( $this, $files );
	}

	public function name(): string {
		return 'Core';
	}

	public function path(): string {
		return $this->basePath;
	}

	public function version(): string {
		return '1.38.0';
	}

	public function extensionJson(): array {
		return [];
	}
}
