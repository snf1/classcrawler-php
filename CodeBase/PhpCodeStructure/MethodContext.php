<?php

namespace Tools\CodeBase\PhpCodeStructure;


use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Name\FullyQualified;

class MethodContext
{
    public array $context;

    public function __construct()
    {
        $this->context = [];
    }

    public function addContext($expression, $type)
    {
        if (!isset($this->context[$expression])) {
            $this->context[$expression] = $type;
        }
    }

    /**
     * @param $expression
     *
     * @return mixed|null
     */
    public function getTypeByName($expression){
        // temporary fix to avoid errors
        if($expression instanceof MethodCall || $expression instanceof Variable || $expression instanceof Identifier ||  $expression instanceof FullyQualified || $expression instanceof Name) {
            return $this->context[$expression->toString()] ?? $this->context['this'];
        }
        // returning $this Type like last place to search if expression not found
        return $this->context[$expression] ?? $this->context['this'];
    }
}