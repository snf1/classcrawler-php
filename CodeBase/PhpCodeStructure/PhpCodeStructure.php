<?php
namespace Tools\CodeBase\PhpCodeStructure;
use Tools\CodeBase\Files\GenericFile;


interface PhpCodeStructure {
    function methods(): array;
    function classes(): array;
}

