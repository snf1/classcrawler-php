<?php
namespace Tools\CodeBase\PhpCodeStructure;

use PhpParser\NameContext;
use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\BinaryOp\Coalesce;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\New_;
use PhpParser\Node\Expr\PropertyFetch;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Name;
use PhpParser\Node\NullableType;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Expression;
use PhpParser\NodeFinder;
use Tools\CodeBase\Files\GenericFile;

use function PHPUnit\Framework\isEmpty;

class PhpMethod {

    /** @var string|null $name */
    private $name;
    /** @var string|null $fullName */
    private $fullName;
    /** @var array $overrides */
    private $overrides;
    /** @var array $overridden */
    private $overridden;
    /** @var PhpVariable[] $arguments */
    private $arguments;
    /** @var string $extensionName */
    private $extensionName;

    /** @var ClassMethod|null $methodNode */
    private $methodNode;

    /** @var PhpClass|null $class */
    private $class;

    /** @var  NameContext|null */
    private $nameContext;

    /** @var GenericFile */
    private $file;

    /**
     * @var array
     */
    private $returnTypes;

    /**
     * @var mixed
     */
    private $tags;

	/**
	 * @var array
	 */
	private $hardDeprecated;

    /**
     * @var array
     */
    private $callees;

    /**
     * @var array
     */
    private $callers;

    function __construct() { }

    public static function constructByNode( ClassMethod $methodNode, PhpClass $class, GenericFile $file, NameContext $nameContext ): PhpMethod {
        $phpMethod = new self();
        $phpMethod->byNode( $methodNode, $class, $file, $nameContext );
        return $phpMethod;
    }

    public static function constructByArray(  $method ): PhpMethod {
        $phpMethod = new self();
        $phpMethod->byArray( $method );
        return $phpMethod;
    }

    protected function byNode( ClassMethod $methodNode, PhpClass $class, GenericFile $file, NameContext $nameContext ) {
        $this->methodNode = $methodNode;
        $this->class = $class;
        $this->nameContext = $nameContext;
        $this->file = $file;
    }

    protected function byArray( $method ) {
        $this->name = $method[ "name" ];
        $this->fullName = $method[ "fullName" ];
        $this->overrides = $method[ "overrides" ];
        $this->overridden = $method[ "overridden" ];
        $this->code = $method[ "code" ];
        $this->returnTypes = $method[ "returnTypes" ];
        $this->tags = $method[ "tags" ];
        $this->hardDeprecated = $method[ "hardDeprecated" ];
        $this->extensionName = $method[ "extension" ];

        foreach( $method[ "arguments" ] as $methodName => $method ) {
            $this->methods[$methodName] = PhpVariable::constructByArray($method);
        }
    }

    function class(): PhpClass {
        return $this->class;
    }

    function name(): string {
        if(isset($this->name)) {
            return $this->name;
        }
        return $this->methodNode->name;
    }

    function extensionName(): string {
        if(isset($this->extensionName)) {
            return $this->extensionName;
        }

        return $this->file->project()->name();
    }

    /**
     * @return string[]
     */
    function returnTypes(): array {
        if (isset($this->returnTypes)) {
            return $this->returnTypes;
        }
        if (isset($this->methodNode->returnType)) {
            $type = $this->methodNode->returnType;
            if ($type instanceof NullableType) {
                $type = $type->type;
            }
            return [$type->__toString()];
        }
        $docComment = $this->methodNode->getDocComment();
        if (isset($docComment)) {
            $docText = $docComment->getText();
            $rx = '/@return (\S*)/';
            if( preg_match( $rx, $docText, $matches )) {
                return explode('|', $matches[1]);
            }
        }

        return [];
    }

    function fullName(): string {
        if(isset($this->fullName)) {
            return $this->fullName;
        }
        return $this->class->fullName() . '::' . $this->name();
    }
    /**
     * @return PhpVariable[]
     */
    function arguments(): array {
        if(isset($this->arguments)) {
            return $this->arguments;
        }
        $res = [];
        foreach($this->methodNode->getParams() as $param) {
            $docComment = $this->methodNode->getDocComment();
            $res[] = PhpVariable::constructByNode( $param, $docComment, $this->nameContext );
        }
        return $res;
    }

    function code() {
        if(isset($this->code)) {
            return $this->code;
        }
        return $this->file->lines($this->methodNode->getStartLine(), $this->methodNode->getEndLine());

    }

    function overrides(): array {
        return $this->overrides;
    }

    function overridden(): array {
        return $this->overridden;
    }

    /**
     * @return array
     */
    function tags(): array {
        if (isset($this->tags)) {
            return $this->tags;
        }

        $docComment = $this->methodNode->getDocComment();
        if (isset($docComment)) {
            $docText = $docComment->getText();
            $rx = '/@(codeCoverageIgnore|deprecated|internal|stable|since|see|todo|unstable|warning) ?(.*)/';
            if( preg_match_all( $rx, $docText, $matches, PREG_SET_ORDER )) {
            foreach ($matches as $match) {
                $res[$match[1]] = $match[2];
            }

                return $res;
            }
        }

        return [];
    }

 	/**
 	 * Check whether a function or method includes a call to wfDeprecated(),
 	 * indicating that it is a hard-deprecated interface.
	 *
 	 * @return array
 	 */
 	public function hardDeprecated(): array {
		if (isset($this->hardDeprecated)) {
			return $this->hardDeprecated;
		}

 		if ( !$this->methodNode->stmts ) {
			return [];
 		}

 		$firstStmt = $this->methodNode->stmts[0];
		if (
			$firstStmt instanceof Expression &&
			$firstStmt->expr instanceof FuncCall &&
			$firstStmt->expr->name instanceof Name &&
			$firstStmt->expr->name->toString() === 'wfDeprecated'

		) {
			if ( count( $firstStmt->expr->args ) < 2 ) {
				return ['status' => true];
			}
			if (
				$firstStmt->expr->args[1]->value instanceof String_ &&
				preg_match('/([0-9]+\.?[0-9]+)/', $firstStmt->expr->args[1]->value->value, $matches)
			) {
				return ['status' => true, 'version' => $matches[0]];
			} else {
				return ['status' => true];
			}
		}
		return [];
	}

    /**
     * @param $processedClasses
     */
    public function callees($processedClasses): void {

        $context = new MethodContext();
        $resolver = new Resolver($processedClasses);
        //foreach element in function we have to add context

        foreach ($this->methodNode->params as $param ){
//            print($this->class->name() . "</n>");
//            print($param->var->name. "</n>");
            $type = null;
                if ($param->type instanceof NullableType) {
                    $type = $param->type->type->toString();
                } elseif ($param->type == null){
                    $docComment = $this->methodNode->getDocComment();
                    if (isset($docComment)) {
                        $docText = str_replace('[]','', $docComment->getText());
                        $rx = '/@param (.*) \$'. $param->var->name . '/';
                        if( preg_match( $rx, $docText, $matches )) {
                            $type = explode('|', $matches[1])[0];
                        }
                    } else {
                        $type = null;
                    }
                } else {
                    $type = $param->type->toString();
                }
                // for future if I'll find out how to store both possible return types
                if (is_array($type)) {
                    foreach ($type as $typeVariation) {
                        $context->addContext($param->var->name, $typeVariation);
                    }
                } else {
                    $context->addContext($param->var->name, $type);
                }
        }
        $context->addContext('this', $this->class->fullName());

        foreach ($this->class->properties() as $property) {
            if (is_array($property->types()) && !empty($property->types())) {
                $context->addContext('this->' . $property->name(), $property->types()[0]);
            } elseif (!is_array($property->types())) {
                $context->addContext('this->' . $property->name(), $property->types());
            }
        }

        $nodeFinder = new NodeFinder;
        $assigns = $nodeFinder->findInstanceOf($this->methodNode, Assign::class);
        $callees = $nodeFinder->findInstanceOf($this->methodNode, MethodCall::class);
        $staticCallees = $nodeFinder->findInstanceOf($this->methodNode, StaticCall::class);
        $callees = array_merge($callees, $staticCallees);

//        $statements = $this->methodNode->getStmts();
//
//        foreach ($statements as $stmt) {
//            $variable = null;
//            if ($stmt->expr instanceof Assign) {
//                $variable = $stmt->expr->var->name;
//            }
//        }

        $combined = array_map(
            function ($callee) use ($assigns) {
                foreach ($assigns as $assign) {
                    if ($callee === $assign->expr && $assign->var instanceof Variable) {
                        $callee->assignedName = $assign->var->name;
                    }
                }
                return $callee;
            },
            $callees);


//        foreach ($callees as $callee) {
//            if (true){}
//        }



        foreach ($callees as $callee) {
//            $this->expressionUnpack($callee, $context, $resolver);
            if ($callee instanceof StaticCall) {
                $exprBefore = $callee->class instanceof Variable ? $callee->class->name : $callee->class->toString();
                ($exprBefore == 'self') ? $context->addContext($exprBefore, $this->class->fullName()) : $context->addContext($exprBefore, $exprBefore);
                $exprAfter = $callee->name->name;
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof StaticCall) {
                $exprBefore = $callee->var->class->toString();
                ($exprBefore == 'self') ? $context->addContext($exprBefore, $this->class->fullName()) : $context->addContext($exprBefore, $exprBefore);
                $exprAfter = $callee->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore) == 'self' ? $callee->var->class->toString() : $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof MethodCall && $callee->var->var instanceof StaticCall) {
                $exprBefore = $callee->var->var->class->toString();
                ($exprBefore == 'self') ? $context->addContext($exprBefore, $this->class->fullName()) : $context->addContext($exprBefore, $exprBefore);
                $exprAfter = $callee->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore) == 'self' ? $callee->var->var->class->toString() : $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore) == 'self' ? $callee->var->var->class->toString() : $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof MethodCall && $callee->var->var instanceof MethodCall && $callee->var->var->var instanceof StaticCall) {
                $exprBefore = $callee->var->var->var->class->toString();
                ($exprBefore == 'self') ? $context->addContext($exprBefore, $this->class->fullName()) : $context->addContext($exprBefore, $exprBefore);
                $exprAfter = $callee->var->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore) == 'self' ? $callee->var->var->var->class->toString() : $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore) == 'self' ? $callee->var->var->var->class->toString() : $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore) == 'self' ? $callee->var->var->var->class->toString() : $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof MethodCall && $callee->var->var instanceof MethodCall && $callee->var->var->var instanceof MethodCall && $callee->var->var->var->var instanceof PropertyFetch) {
                $exprBefore = $callee->var->var->var->var->var->name;
                $exprAfter = $callee->var->var->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof MethodCall && $callee->var->var instanceof MethodCall && ($callee->var->var->var instanceof MethodCall || $callee->var->var->var instanceof PropertyFetch)) {
                $exprBefore = $callee->var->var->var->var->name;
                $exprAfter = $callee->var->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof MethodCall && ($callee->var->var instanceof MethodCall || $callee->var->var instanceof PropertyFetch)) {
                $exprBefore = $callee->var->var->var instanceof New_ ? $callee->var->var->var->class->toString() : $callee->var->var->var->name;
                $exprAfter = $callee->var->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof MethodCall && $callee->var->var instanceof New_) {
                $exprBefore = $callee->var->var->class->toString();
                $context->addContext($exprBefore, $exprBefore);
                $exprAfter = $callee->name->name;
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && ($callee->var instanceof PropertyFetch || $callee->var instanceof MethodCall)) {
                $exprBefore = $callee->var->var instanceof ArrayDimFetch ? $callee->var->var->var->name : $callee->var->var->name;
                $exprAfter = $callee->var->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $exprBefore . '->' . $exprAfter;
                $context->addContext($name, $resolvedType);

                $exprBefore = $name;
                $exprAfter = $callee->name instanceof Variable ? $callee->name->name : $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof ArrayDimFetch && $callee->var->var instanceof ArrayDimFetch) {
                $exprBefore = $callee->var->var->var->name;
                $exprAfter = $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee instanceof MethodCall && $callee->var instanceof ArrayDimFetch) {
                $exprBefore = $callee->var->var->name;
                $exprAfter = $callee->name instanceof Variable ? $callee->name->name : $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee->var instanceof New_) {
                $exprBefore = $callee->var->class->toString();
                $context->addContext($exprBefore, $exprBefore);
                $exprAfter = $callee->name->name;
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee->var instanceof FuncCall) {
                $exprBefore = $callee->var->name instanceof Name ? $callee->var->name->toString() : $callee->var->name->name;
                $exprAfter = $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            } elseif ($callee->var instanceof Coalesce) {
                $name = $this->class->fullName();
                $resolvedType = $this->class->fullName();
            } else {
                $exprBefore = $callee->var->name;
                $exprAfter = $callee->name instanceof Variable ? $callee->name->name : $callee->name->toString();
                $typeBefore = $context->getTypeByName($exprBefore);
                $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
                $name = $typeBefore . '::' . $exprAfter;
            }
            $context->addContext($name, $resolvedType);
            isset($callee->assignedName) ? $context->addContext($callee->assignedName, $resolvedType) : null;
        }

        $this->callees = array_filter($context->context, function($k) {
            return str_contains($k, '::') ?? $k;
        }, ARRAY_FILTER_USE_KEY);
        $this->callees = array_keys($this->callees);
        $this->callees = $this->callees != null ? $this->callees : [];
    }

    /**
     * @return array|null
     */
    public function getCallees() : ?array
    {
        return $this->callees;
    }

    public function addCaller($methodName) {

        $this->callers[] = $methodName;
    }

    /**
     * @return array|null
     */
    public function getCallers() : ?array
    {
        return $this->callers;
    }
//
//    /**
//     * Recursion to get all fields inside callee.
//     * Will be made soon.
//     *
//     * @param $callee
//     * @param $context
//     * @param $resolver
//     */
//    protected function expressionUnpack(&$callee, &$context, $resolver)
//    {
//        if ($callee instanceof StaticCall) {
//            $exprBefore = $callee->class instanceof Variable ? $callee->class->name : $callee->class->toString();
//            ($exprBefore == 'self') ? $context->addContext($exprBefore, $this->class->fullName()) : $context->addContext($exprBefore, $exprBefore);
//            $exprAfter = $callee->name->name;
//            $typeBefore = $context->getTypeByName($exprBefore);
//            $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
//            $name = $typeBefore . '::' . $exprAfter;
//
//            $context->addContext($name, $resolvedType);
//        } elseif ($callee instanceof MethodCall) {

            // get var->name

//            $exprBefore = $callee->var->name;
//            $exprAfter = $callee->var->var->var->var->name->toString();
//            $typeBefore = $context->getTypeByName($exprBefore);
//            $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
//            $name = $exprBefore . '->' . $exprAfter;
//            $context->addContext($name, $resolvedType);

//            $this->expressionUnpack($callee->var, $context, $resolver);
//        } else {
//            $exprBefore = $callee->var->name;
//            $exprAfter = $callee->name instanceof Variable ? $callee->name->name : $callee->name->toString();
//            $typeBefore = $context->getTypeByName($exprBefore);
//            $resolvedType = $resolver->getReturnType($typeBefore, $exprAfter, $callee);
//            $name = $typeBefore . '::' . $exprAfter;
//
//            $context->addContext($name, $resolvedType);
//        }
//
//    }


}
