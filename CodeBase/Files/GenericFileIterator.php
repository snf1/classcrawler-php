<?php

namespace Tools\CodeBase\Files;

use FilterIterator;
use Iterator;
use RecursiveIterator;
use Tools\CodeBase\Project;
use RecursiveFilterIterator;
use Tools\CodeBase\Files\GenericFile;

class GenericFileIterator extends FilterIterator {

	/** @var Project */
	private $project;
	public function __construct( Project $project, Iterator $iterator) {
		parent::__construct($iterator);
		$this->project = $project;
	}
	public function accept(): bool {
		return parent::current()->isFile();
	}

	public function current() {
		if ( preg_match( '/\.php$/', parent::current()->getPathname() ) ) {
			return new PhpFile(parent::current()->getPathname(), $this->project );
		}

		return new GenericFile( parent::current()->getPathname(), $this->project );
	}
}
