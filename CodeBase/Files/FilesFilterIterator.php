<?php

namespace Tools\CodeBase\Files;

use Iterator;
use Exception;
use Traversable;
use FilterIterator;
use RecursiveFilterIterator;
use Tools\CodeBase\File;
use Tools\FileFilters\FileFilter;
use Tools\FileFilters\AnyFileFilter;
use Tools\FileFilters\CombineFileFilter;



class FilesFilterIterator extends FilterIterator {

	private $fileFilter = null;

	public function __construct( Traversable $files, $fileFilter = null ) {
		parent::__construct( $files );
		$this->fileFilter = $fileFilter ?? new AnyFileFilter();
	}

	public function filter( object $filter ) {
		if ( $filter instanceof FileFilter ) {
			return new FilesFilterIterator(
				$this->getInnerIterator(),
				new CombineFileFilter( $filter, $this->fileFilter )
			);
		}
		throw new Exception( 'Invalid filter' );
	}

	public function accept(): bool {
		$current = parent::current();
		return ( $current instanceof File &&
			$this->fileFilter->test( $current ) );
	}
}
