<?php

namespace Tools\CodeBase\Files;

use PhpParser\Error;
use PhpParser\NodeFinder;
use PhpParser\NameContext;
use PhpParser\ErrorHandler;
use Tools\CodeBase\Project;
use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;
use PhpParser\Node\Stmt\ClassLike;
use Tools\CodeBase\Files\GenericFile;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\NodeVisitor\FindingVisitor;
use Tools\CodeBase\PhpCodeStructure\PhpClass;
use Tools\CodeBase\PhpCodeStructure\PhpCodeStructure;

class PhpFile extends GenericFile implements PhpCodeStructure, ErrorHandler {

    public function __construct(string $basePath, Project $project) {
        parent::__construct($basePath, $project);
    }

    public function handleError(Error $error) {
        
    }

    // TODO: Write doc types
    /**
     * @return PhpClass[]
     */
    public function classes() : array
    {
        $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
        
        $nameResolver = new NameResolver(null, [
            'preserveOriginalNames' => false,
            'replaceNodes' => true,
        ]);
        $nodeTraverser = new NodeTraverser;
        $nodeTraverser->addVisitor($nameResolver);
       
        $stmts = $parser->parse($this->content());
        $stmts = $nodeTraverser->traverse($stmts);

        $nodeFinder = new NodeFinder;
        $classNodes = $nodeFinder->findInstanceOf($stmts, ClassLike::class);
        
        $result = [];
        foreach ($classNodes as $classNode) {
            $result[] = PhpClass::constructByNode( $classNode, $this, $nameResolver->getNameContext() );
        }
        return $result;
    }

    public function methods() : array
    {
        return [];
    }
}