<?php

namespace Tools\FileFilters;

use Tools\CodeBase\File;
use Tools\FileFilters\FileFilter;

class CombineFileFilter implements FileFilter {
	private $left;
	private $right;

	public function __construct( FileFilter $left, FileFilter $right ) {
		$this->left = $left;
		$this->right = $right;
	}

	public function test( File $file ): bool {
		return $this->left->test( $file ) && $this->right->test( $file );
	}
}