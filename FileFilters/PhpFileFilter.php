<?php

namespace Tools\FileFilters;
use Tools\CodeBase\File;
use Tools\CodeBase\Files\PhpFile;
use Tools\FileFilters\FileFilter;



class PhpFileFilter implements FileFilter {
	public function test( File $file ): bool {
		return $file instanceof PhpFile;
	}
}