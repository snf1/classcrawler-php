<?php

namespace Tools\FileFilters;
use Tools\CodeBase\File;

interface FileFilter {
	public function test( File $file ): bool;
}
