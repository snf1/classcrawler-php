<?php
declare(strict_types=1);

use Tools\CodeBase\PhpCodeStructure\PhpClass;
use Tools\CodeBase\Project;
use PHPUnit\Framework\TestCase;
use Tools\CodeBase\Files\PhpFile;

final class PhpFileTest extends TestCase {

    public function testPhpFileHandlesClass(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestClass.php', $this->createMock(Project::class));
        $this->assertEquals(1, count($phpFile->classes()));
        $phpClass = $phpFile->classes()[0];

        $this->assertTrue($phpClass instanceof PhpClass);
    }

    public function testPhpFileHandlesTwoClasses(): void {
        $phpFile = new PhpFile('../playground/TestFiles/TestTwoClasses.php', $this->createMock(Project::class));
        $this->assertEquals(2, count($phpFile->classes()));

        foreach ($phpFile->classes() as $phpClass ) {
            $this->assertTrue($phpClass instanceof PhpClass);
        }
    }

    public function testPhpFileConstructor(): void {
        $basePath = '../playground/TestFiles/TestClass.php';
        $project = $this->createMock(Project::class);
        $phpFile = new PhpFile($basePath, $project);

        $this->assertSame($basePath, $phpFile->path());
        $this->assertSame($project, $phpFile->project());
    }

}
