<?php

namespace Tools\CodeBase;

require_once __DIR__ . '/../Maintenance.php';
require_once __DIR__ . '/../../vendor/autoload.php';

use Maintenance;

class TestGeneration extends Maintenance {
	public function __construct() {
		parent::__construct();
		$this->addDescription( 'Generating tests for specific class' );
	}

	public function execute() {
		global $IP;

		$generator = new TestGenerator();
		$code = $generator->generateTest( $IP . '/includes/content/ContentHandler.php' );
		$res = @mkdir( $IP . '/tests/phpunit/includes/content/generated', 0777, true );
		file_put_contents(
			$IP . '/tests/phpunit/includes/content//generated/ContentHandlerGeneratedTest.php',
			$code
		);
	}
}

$maintClass = TestGeneration::class;
require_once RUN_MAINTENANCE_IF_MAIN;
