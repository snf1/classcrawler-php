<?php

namespace Tools\CodeBase;

class UnderReview implements ProjectFilter {
	private $branch;

	public function __construct( string $branch ) {
		$this->branch = $branch;
	}

	public function test( Project $extension ): bool {
		return ( new GitBlock( $extension->path() ) )->isUnderReview( $this->branch );
	}
}
