<?php

namespace Tools\ProjectFilters;

use Tools\CodeBase\Project;
use Tools\CodeBase\ProjectFilter;

class CombineProjectFilter implements ProjectFilter {
	private $left;
	private $right;

	public function __construct( ProjectFilter $left, ProjectFilter $right ) {
		$this->left = $left;
		$this->right = $right;
	}

	public function test( Project $project ): bool {
		return $this->left->test( $project ) && $this->right->test( $project );
	}
}